package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		jugadores = pJugadores;
		
		
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	public ArrayList<Jugador> darListaJugadores()
	{
		return jugadores;
	}
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		boolean pinte = false;
		for (int i = 0; i < tablero.length; i++) 
		{
			for (int j = 0; j < tablero[0].length && !pinte; j++)
			{
				if(tablero[i][j].equals("___"))
				{
					tablero[i][j] = "%"; 
					pinte = true;
				}
			}
		}
	}
	
	public String buscarSimboloPorNombre(String pNombre)
	{
		String simb = null;
		for (int i = 0; i < jugadores.size(); i++) {
			Jugador el = jugadores.get(i);
			if(el.darNombre().compareTo(pNombre) == 0)
			{
				simb = el.darSimbolo();
			}
		}
		
		return simb;
	}
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int fil, int col, String pSimbolo)
	{
		//TODO
		boolean puede = false;
		
		if(fil<tablero.length && col<tablero[0].length &&  tablero[fil][col].equals("___"))
		{
			tablero[fil][col] = "_"+pSimbolo+"_";
			puede = true;
		}
		return puede;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col, String pSimbolo, int pSimboloSeguidoGanar)
	{
		//TODO
		boolean termino = false;
		
		if(validarHorizontalVertical(fil, col, pSimbolo, pSimboloSeguidoGanar) == true)
		{
			termino = true;
		}
		else
		{
			int filas =  tablero.length;
			int columnas = tablero[0].length;
			
			for (int i = 0; i < filas; i++)
			{
				for (int j = 0; j < columnas; j++) 
				{
					
				}
			}
		}
		
		return termino;
	}
	
	public boolean validarHorizontalVertical(int fil,int col, String pSimbolo, int pSimboloSeguidoGanar)
	{
		boolean termino = false;
		int igualdadVertical = 0;
		int igualdadHorizontal = 0;
		int filas =  tablero.length;
		int columnas = tablero[0].length;
		
		for (int i = 0; i < filas; i++)
		{
			if(tablero[i][col].compareTo(pSimbolo) == 0)
			{
				igualdadVertical++;
			}
		}
		
		for (int j = 0; j < columnas; j++) 
		{
			if(tablero[fil][j].compareTo(pSimbolo) == 0)
			{
				igualdadHorizontal++;
			}
		}
		
		if( (igualdadHorizontal == pSimboloSeguidoGanar) || (igualdadVertical == pSimboloSeguidoGanar))
		{
			termino = true;
		}
		
		return termino;
	}

}
