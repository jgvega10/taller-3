package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */

	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		//TODO

		System.out.println("Por favor introduzca el numero de jugadores");

		int numeroJug = sc.nextInt();
		sc.nextLine();

		System.out.println("Por favor escriba los nombres de los jugadores");

		ArrayList<String> nom = new ArrayList<String>();

		for (int i = 0; i < numeroJug; i++) 
		{	
			String nombre = sc.nextLine();

			nom.add(nombre);
		}

		ArrayList<Jugador> listaJugadores = new ArrayList<>();

		for (int i = 0; i < nom.size(); i++) 
		{
			String nombreDelJugador = nom.get(i);

			System.out.println("Por favor   -" + nombreDelJugador+"-    escriba el simbolo con el cual desea jugar");

			String simbolo = sc.nextLine();

			Jugador a = new Jugador(nombreDelJugador, simbolo);

			listaJugadores.add(a);


		}

		System.out.println("Introduzca el numero de filas que desea para el juego");

		int tamaño = sc.nextInt();

		boolean ter = true;

		while(ter)
		{
			if(tamaño < 4)
			{
				System.out.println("El tamaño debe ser mayor o igual 4");
				tamaño = sc.nextInt();
			}
			else
			{
				ter = false;
			}
		}

		System.out.println("Introduzca el numero de columnas que desea para el juego");

		int tamañoCol = sc.nextInt();
		sc.nextLine();

		boolean terCol = true;

		while(terCol)
		{
			if(tamañoCol < 4)
			{
				System.out.println("El tamaño debe ser mayor o igual 4");
				tamañoCol = sc.nextInt();
			}
			else
			{
				terCol = false;
			}
		}

		LineaCuatro elJuego = new LineaCuatro(listaJugadores, tamaño, tamañoCol);

		imprimirTablero(elJuego);
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego(LineaCuatro pJuego)
	{
		//TODO
		System.out.println("Indique la cantidad de simbolos seguidos que se deben tener para ganar el juego");
		System.out.println("IMPORTANTE: LA CANTIDAD DE SIMBOLOS SEGUIDOS DEBE SER MENOR AL NUMERO MENOR ENTRE EL ANCHO Y EL ALTO DE LA MATRIZ y mayor a 2");

		boolean seguir = true;
		int simbolosSeguidos = 0;

		while(seguir)
		{
			simbolosSeguidos = sc.nextInt();
			if(simbolosSeguidos < 3)
			{
				System.out.println("El numero debe ser mayor a 2");
			}
			else
			{
				seguir = false;
			}
		}



		sc.nextLine();

		boolean finJuego = false;
		ArrayList<Jugador> juga = pJuego.darListaJugadores();

		int k = 0;

		while(!finJuego)
		{
			String atac = juga.get(k).darNombre();


			System.out.println();
			System.out.println("IMPORTANTE:  EL CONTEO DE LAS FILAS Y LAS COLUMNAS EMPIEZA DESDE CERO");
			System.out.println();
			System.out.println("Atacante:   "+atac);
			System.out.println("Por favor digite el numero de la fila en la cual desea jugar");

			int filaJugada = sc.nextInt();

			System.out.println("Por favor digite el numero de la columna en la cual desea jugar");

			int columnaJugada = sc.nextInt();

			boolean poder = pJuego.registrarJugada(filaJugada, columnaJugada, pJuego.buscarSimboloPorNombre(atac));

			if(poder == true)
			{
				for (int i = 0; i < pJuego.darTablero().length; i++)
				{
					for (int j = 0; j < pJuego.darTablero()[0].length; j++) 
					{
						if(i == filaJugada && (j == columnaJugada) )
						{
							String sim = pJuego.buscarSimboloPorNombre(atac);
							System.out.print("_"+sim+"_"+ "   ");

							if(pJuego.terminar(filaJugada, columnaJugada, pJuego.buscarSimboloPorNombre(atac), simbolosSeguidos))
							{
								finJuego = true;
								System.out.println();
								System.out.println("EL GANADOR ES:" + "  "+atac);
							}
							else
							{
								if(k < juga.size() -1)
								{
									k++;
								}
								else
								{
									k = 0;
								}
							}

						}
						else
						{
							System.out.print(pJuego.darTablero()[i][j] + "   ");
						}
					}

					System.out.println();
					System.out.println();
				}
			}
			else
			{
				System.out.println("LAS COORDENADAS SON INVALIDAS, por favor vuelva a intentarlo");
				System.out.println();
				for (int i = 0; i < pJuego.darTablero().length; i++)
				{
					for (int j = 0; j < pJuego.darTablero()[0].length; j++)
					{

						System.out.print(pJuego.darTablero()[i][j]+ "   ");
					}

					System.out.println();
					System.out.println();
				}
			}
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		ArrayList<Jugador> a = new ArrayList<>();

		System.out.println("Por favor introduzca su nombre");
		String nombre = sc.nextLine();
		sc.nextLine();

		System.out.println("Por favor escriba el simbolo que usara para jugar");
		String simbolo = sc.nextLine();

		Jugador jugador = new Jugador(nombre, simbolo);
		a.add(jugador);

		System.out.println("Introduzca el numero de filas que desea para el juego");

		int tamaño = sc.nextInt();

		boolean ter = true;

		while(ter)
		{
			if(tamaño < 4)
			{
				System.out.println("El tamaño debe ser mayor o igual 4");
				tamaño = sc.nextInt();
			}
			else
			{
				ter = false;
			}
		}

		System.out.println("Introduzca el numero de columnas que desea para el juego");

		int tamañoCol = sc.nextInt();
		sc.nextLine();

		boolean terCol = true;

		while(terCol)
		{
			if(tamañoCol < 4)
			{
				System.out.println("El tamaño debe ser mayor o igual 4");
				tamañoCol = sc.nextInt();
			}
			else
			{
				terCol = false;
			}
		}


		LineaCuatro jue = new LineaCuatro(a, tamaño, tamañoCol);

		imprimirTableroMaquina(jue);
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina(LineaCuatro pJuego)
	{
		//TODO
		System.out.println("Indique la cantidad de simbolos seguidos que se deben tener para ganar el juego");
		System.out.println("IMPORTANTE: LA CANTIDAD DE SIMBOLOS SEGUIDOS DEBE SER MENOR AL NUMERO MENOR ENTRE EL ANCHO Y EL ALTO DE LA MATRIZ y mayor a 2");

		boolean seguir = true;
		int seguidos = 0;
		while(seguir)
		{
			seguidos = sc.nextInt();
			if(seguidos < 3)
			{
				System.out.println("El numero debe ser mayor a 2");
			}
			else
			{
				seguir = false;
			}
		}

		boolean finJuego = pJuego.fin();
		ArrayList<Jugador> juga = pJuego.darListaJugadores();
		boolean intercalar = false;
		String atacante = juga.get(0).darNombre();
		while(!finJuego)
		{
			if(!intercalar)
			{
				System.out.println("Atacante:"+ " " + atacante);
				System.out.println("Introduzca el numero de la fila en la cual desea jugar");
				int fila = sc.nextInt();
				sc.nextLine();

				System.out.println("Introduzca el numero de la columna en la cual desea jugar");
				int columna = sc.nextInt();

				boolean poder = pJuego.registrarJugada(fila, columna, pJuego.buscarSimboloPorNombre(atacante));

				if(poder == true)
				{
					for (int i = 0; i < pJuego.darTablero().length; i++)
					{
						for (int j = 0; j < pJuego.darTablero()[0].length; j++) 
						{
							if(i == fila && (j == columna) )
							{
								String sim = pJuego.buscarSimboloPorNombre(atacante);
								System.out.print("_"+sim+"_"+ "   ");
								intercalar = true;
								if(pJuego.terminar(fila, columna, sim, seguidos))
								{
									System.out.println();
									System.out.println("EL GANADOR ES:" + "  "+atacante);

								}
								
							}
							else
							{
								System.out.print(pJuego.darTablero()[i][j] + "   ");
							}
							
						}
						System.out.println();
					}
				}
				
			}
			else
			{
				pJuego.registrarJugadaAleatoria();
				
				String[][] maq = pJuego.darTablero();
				
				System.out.println();
				System.out.println();
				for (int i = 0; i < maq.length; i++) 
				{
					for (int j = 0; j < maq[0].length; j++) 
					{
						System.out.print("_"+ maq[i][j] + "_   ");
					}
					
					System.out.println();
					System.out.println();
				}
				intercalar = false;
			}

		}

	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero(LineaCuatro pLineaCuatro)
	{
		//TODO
		String[][] table = pLineaCuatro.darTablero();

		for (int i = 0; i < table.length; i++) 
		{
			for (int j = 0; j < table[0].length; j++)
			{
				System.out.print(table[i][j] + "  ");
			}

			System.out.println();
			System.out.println();

		}

		juego(pLineaCuatro);
	}
	
	public void imprimirTableroMaquina(LineaCuatro pJuego)
	{
	
		String[][] table = pJuego.darTablero();

		for (int i = 0; i < table.length; i++) 
		{
			for (int j = 0; j < table[0].length; j++)
			{
				System.out.print(table[i][j] + "  ");
			}

			System.out.println();
			System.out.println();

		}

		juegoMaquina(pJuego);
	}
}
