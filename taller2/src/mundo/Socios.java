package mundo;

import java.util.Date;

public class Socios extends Trabajo
{
	protected String nombreEmpresaDelSocio;
	
	public Socios(Date pFecha, String pLugar, TipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal, String empresaSocio)
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		nombreEmpresaDelSocio = empresaSocio;

	}
	
	public String prue(Boolean b)
	{
		String me = "";
		if(!b)
		{
			me = "No";
		}
		else
		{
			me = "Si";
		}
		
		return me;
	}
	public String toString()
	{
	  return "Evento con SOCIO: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nNOMBRE DE LA EMPRESA QUE HACE PARTE EL SOCIO: " + this.nombreEmpresaDelSocio
	  		+"\nOBLIGATORIO: " + prue(this.obligatorio)
	  		+"\nFORMAL: "+ prue(this.formal);
	}
}
