package mundo;

import java.util.Date;

public class Clientes extends Trabajo
{
	
	protected  String  tipoNegocio;

	public Clientes(Date pFecha, String pLugar, TipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal, String pTipoNegocio) 
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		tipoNegocio = pTipoNegocio;
	}
	
	public String prue(Boolean b)
	{
		String me = "";
		if(!b)
		{
			me = "No";
		}
		else
		{
			me = "Si";
		}
		
		return me;
	}
	public String toString()
	{
	  return "Evento con CLIENTE: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nNOMBRE DEL NEGOCIO QUE TIENE CON EL CLIENTE: " + this.tipoNegocio
	  		+"\nOBLIGATORIO: " + prue(this.obligatorio)
	  		+"\nFORMAL: "+ prue(this.formal);
	}

}
