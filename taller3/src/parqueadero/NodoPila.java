
package parqueadero;

public class NodoPila<T>
{
	private T elemento;
	
	private NodoPila siguiente;
	
	public NodoPila(T pElemento)
	{
		elemento = pElemento;
		siguiente = null;
	}
}
